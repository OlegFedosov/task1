<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<p class="message"><?php echo $message ; ?></p>

<form  role="form" method="post" enctype="multipart/form-data" class="form-inline">
    <div class="form-group">
        <label for="file">File Upload</label>
        <input id="file" type="file" name="filename"/>
    </div>
    <button type="submit" class="btn">push to upload</button>
</form>

<?php if (is_array($filesList) and count($filesList)): ?>
        <table class="table">
            <thead>
            <tr>
                <th>№</th>
                <th>File Name</th>
                <th>File Size</th>
                <th>Delete file from server</th>
            </tr>
            </thead>
            <tbody>
                    <?php $i = 0; ?>
                    <?php foreach ($filesList as $item): ?>
                    <tr>
                        <td><?php echo ++$i; ?></td>
                        <td><?php echo $item['name']; ?></td>
                        <td><?php echo $item['size']; ?></td>
                        <td><a href="?file_delete=<?php echo $item['name']; ?>">DELETE</a></td>
                    </tr>
                    <?php endforeach; ?>
            </tbody>
        </table>

<?php else: ?>
    <?php echo EMPTYFILESDIR; ?>
<?php endif; ?>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>











