<?php
ini_set('error_reporting', E_ALL);
session_start();
include('config.php');
include('libs/functions.php');

if (isset($_FILES['filename']) && !empty($_FILES['filename'])) {
    if (uploadFile()) {
        setMessage(UPLOADSUCCESS);
    } else {
        setMessage(UPLOADFALL);
    }
}

if (isset($_GET['file_delete']) and !empty($_GET['file_delete'])) {
    if (deleteFile($_GET['file_delete'])) {
        setMessage(DELETESUCCESS);
        header('Location: /');
        exit;
    } else {
        setMessage(DELETEFALL);
    }
}
   
$filesList = getFilesList();
$message = getMessage();

include('templates/index.php');
