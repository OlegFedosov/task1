<?php
function uploadFile() {
    if (is_uploaded_file($_FILES["filename"]["tmp_name"])){
        $fileName = $_FILES["filename"]["name"];
        if (is_file(DIRPATH . $fileName)) {
            $oldName = explode(".", $fileName);
            $fileName = $oldName[0] . time() . "." . end($oldName);
        }
        return (move_uploaded_file($_FILES["filename"]["tmp_name"], DIRPATH . $fileName));
   } else {
        return false;
   }
}

function getFilesList() {
    if (is_dir(DIRPATH)) {
        if ($sd = scandir(DIRPATH) and is_array($sd) and count($sd)) {
            array_walk($sd, function($file, $key) use(&$new) {
                if (is_file(DIRPATH . $file) and !in_array($file, ['..', '.'])) {
                    $new[$key] = ['name' => $file, 'size' => getFileSize($file)];
                }
            });
            return $new;
        } else {
            return false;
        }      
    }
    return false;
}

function getFileSize($file) {
    $fs = filesize(DIRPATH . $file);
    switch ($fs) {
        case ($fs < 1024);
            return number_format($fs, 2, ',', '') . ' b';
        case ($fs >= 1024 && $fs < 1048576);
            return number_format(($fs/1024), 2, ',', '') . ' kb';
        case ($fs >= 1048576);
            return number_format(($fs/1048576), 2, ',', '') . ' Mb';
        default: return 0;
    }
}

function deleteFile($fileName) {
    if (is_file(DIRPATH . $fileName)) {
        return unlink(DIRPATH . $fileName);
    }
    return false;
}

function setMessage($message) {
    $_SESSION['GLOBAL_MESSAGE'] = $message;
}

function getMessage() {

    if (isset($_SESSION['GLOBAL_MESSAGE'])) {
        $mes = $_SESSION['GLOBAL_MESSAGE'];
        unset($_SESSION['GLOBAL_MESSAGE']);
        return $mes;
    }
    return false;
}